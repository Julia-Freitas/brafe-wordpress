/*
Template Name: Home Page
Theme URI: www.brafe.com
Author: IN Junior
Author URI: www.injunior.com.br
Description: Esse tema foi criado para dar aula no treinamento 2020.2
Version: 1.0
*/


<?php get_header(); ?>

<main>
    <section class="intro">
        <h1>CAFÉS COM A CARA</h1>
        <h1>DO BRASIL</h1>
        <h1> - </h1>
        <h1> Direto das fazendas de Minas Gerais</h1>
    </section>

    <!-- fotos amor e perfeicao-->
    <section class="sobre">
        <h2> Uma Mistura de </h2>
        <div class="container">
            <ul>
                <li>
                    <figure><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/cafe-1.jpg" alt="Foto Latte Art simbolizando amor"></figure>
                </li>
                <li>
                    <figure><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/cafe-2.jpg" alt="Foto Latte Art simbolizando perfeição"></figure>
                </li>
            </ul>
        </div>
    </section>

    <section class="produtos">
        <div class="container">
            <ul>
                <li>
                    <button></button>
                    <h3>Paulista</h3>
                    <h2>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo noutras regiões: o João Alberto Castelo Branco trouxe mudas do Pará</h2>
                </li>
                <li>
                    <button></button>
                    <h3>Carioca</h3>
                    <h2>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo noutras regiões: o João Alberto Castelo Branco trouxe mudas do Pará</h3>
                </li>
                <li>
                    <button></button>
                    <h3>Mineiro</h3>
                    <h2>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo noutras regiões: o João Alberto Castelo Branco trouxe mudas do Pará</h3>
                </li>
            </ul>

            <button>
                SAIBA MAIS
            </button>
        </div>
    </section>

    <section class="portfolio">
        <div class="container">
            <ul>
                <li>
                    <figure><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/botafogo.jpg" alt="imagem loja botafogo"></figure>
                    <h3>Botafogo </h3>
                    <h2>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo.
                    </h2>
                    <button>VER MAPA</button>
                </li>
                <li>
                    <figure><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/iguatemi.jpg" alt="imagem loja iguatemi"></figure>
                    <h3>Iguatemi</h3>
                    <h2>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo.</h2>
                    <button>VER MAPA</button>
                </li>
                <li>
                    <figure><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/mineirao.jpg" alt="imagem loja mineirao"></figure>
                    <h3>Mineirão</h3>
                    <h2>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo.</h2>
                    <button>VER MAPA</button>
                </li>
            </ul>
        </div>
    </section>

    <section class="newsletter">
        <div class="container">
            <p>Assine Nossa Newsletter!</p>
            <p> <em>Promoções e eventos mensais</em></p>

            <p> Digite seu e-mail</p>
            <button>    </button>
        </div>
    </section>
</main>
<?php get_footer(); ?>