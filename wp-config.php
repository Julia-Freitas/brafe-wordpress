<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'M1lV9gabrc2cXCPazUeaKeGtYjDRa1EU4Fd1FUzDCzkqDufSEJuGv5lyHt2lSujpdNqKaBykOHree8xeqsAoSg==');
define('SECURE_AUTH_KEY',  'cmTMw0XsUIeavYmwLFU1f0EQ/WosUprRz7tE/w6C2lkbup5R/DngBia0y/DnvThdJu9IDXkzWqe+eOJCCvDz/A==');
define('LOGGED_IN_KEY',    'uoYVE+UMJkI/wxvZFo1jqdXr1T5MpLPsxtLfANGVo7To0u5Q8XQJfJhZ+C6NaNzWoxE8uWKnyXXfmWDIRnO/rQ==');
define('NONCE_KEY',        'iCWMWfl4bGasWn2mO15nb3ISRdeyKReKrIf4oS8b6+rzZE8QAYfYiQ5N8Becep+g6miGhZ+hWe2N8Zb4Z1ci7g==');
define('AUTH_SALT',        'abeX0BBRrFtfoPIrN0GhMlruKdId1SGDyvK5QYMRaLnd02tMtZuuHRL9m8v+xp1C2BGEEkGV0H7fPszUq03DBQ==');
define('SECURE_AUTH_SALT', 'xKlT5zAj5AclCcS8ahSEsySyxuBagkyZgB9QVv0kSH4rMXpdsyM+d5BGrNuxGYyg//+6+mArcEQDyd5pugpfkg==');
define('LOGGED_IN_SALT',   '8uU+swvsIhxqFH6VlDNgGzbrqKEdIm1M9KWCjPJK+YHwD9eOLwj7ZUTFZRAstfbfOqbSXYB1GoxK8uySrIv01g==');
define('NONCE_SALT',       '9AMy8KKQShytAM+UlnigilyrIWPIrCaNXjh3BVs7crR/zUohRL8keJE6OhdtkuCikOT3zivDHfcJvKOkFWYJ8w==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
